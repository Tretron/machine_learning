# -*- coding: utf-8 -*-
"""
Created on Wed Feb 19 09:34:18 2020

@author: laptop laurens
"""

from __future__ import print_function

import os
import subprocess

import pandas as pd
import numpy as np
from sklearn.tree import DecisionTreeClassifier, export_graphviz

import sys
sys.path.append('../')
from pandas_to_decision_tree import get_csv, encode_target

# Load dataset
df = get_csv("iris.csv")
print("* iris types:", df["name"].unique(), sep="\n")

df2, targets = encode_target(df, "name")
print("* df2.head()", df2[["Target", "name"]].head(),
      sep="\n", end="\n\n")
print("* df2.tail()", df2[["Target", "name"]].tail(),
      sep="\n", end="\n\n")
print("* targets", targets, sep="\n", end="\n\n")

features = list(df2.columns[:4])
print("* features:", features, sep="\n")

y = df2["Target"]
X = df2[features]
dt = DecisionTreeClassifier(min_samples_split=20, random_state=99)
dt.fit(X, y)

prediction = targets[dt.predict([[8,3,6,2]])]

print(prediction)
