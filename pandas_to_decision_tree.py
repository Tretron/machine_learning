# -*- coding: utf-8 -*-
"""
Created on Wed Feb 19 10:40:18 2020

@author: laptop laurens
"""
from __future__ import print_function

import os
import subprocess
import pandas as pd
import numpy as np

def get_csv(file_path):
    """Get the csv, from local csv."""
    if os.path.exists(file_path):
        print("-- iris.csv found locally")
        df = pd.read_csv(file_path, index_col=0)
    else:
        df = 0
        print ("no data found")
    return df

def encode_target(df, target_column):
    """Add column to df with integers for the target.

    Args
    ----
    df -- pandas DataFrame.
    target_column -- column to map to int, producing
                     new Target column.

    Returns
    -------
    df_mod -- modified DataFrame.
    targets -- list of target names.
    """
    df_mod = df.copy()
    targets = df_mod[target_column].unique()
    map_to_int = {name: n for n, name in enumerate(targets)}
    df_mod["Target"] = df_mod[target_column].replace(map_to_int)

    return (df_mod, targets)